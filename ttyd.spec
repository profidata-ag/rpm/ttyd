Name:       ttyd
Version:    1.6.3
Release:    3%{?dist}
Summary:    Share your terminal over the web

License:    MIT
URL:        https://tsl0922.github.io/ttyd/
Source0:    https://github.com/tsl0922/%{name}/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:  gcc-c++
BuildRequires:  json-c-devel
BuildRequires:  vim-common
BuildRequires:  cmake
BuildRequires:  libuv
BuildRequires:  openssl-devel
BuildRequires:  libwebsockets-devel
BuildRequires:  zlib-devel

%description
ttyd is a simple command-line tool for sharing terminal over the web,
inspired by GoTTY.

%prep
%autosetup

%build
%cmake
%cmake_build

%install
%cmake_install

%files
%license LICENSE
%doc README.md
%{_bindir}/ttyd
%{_mandir}/man1/ttyd.1.*

%changelog
* Mon Feb 21 2022 Milivoje Legenovic <milivoje.legenovic@profidata.com> - 1.6.3-3
- xdev-6 build

* Tue Jun 29 2021 Levent Demirörs <levent.demiroers@profidata.com> - 1.6.3-2
- Build with GCC 10
